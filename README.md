# Byron Snippets

Snippets to declare and write Byron's commands, handlers and hooks

## Installation

_Either_

- click the extensions button (lowest square icon in the editor), and type in Byron Snippets, select the one by *leolanavo*

_or_

- go here [vscode Extensions Marketplace](https://marketplace.visualstudio.com/items?itemName=leolanavo.byron-snippets)

```javascript
ext install Byron Snippets
```

You can enable tab completion (recommended) by opening `Code > Preferences > Settings` (on a Mac) and applying `"editor.tabCompletion": "onlySnippets"` to your personal settings

## Snippets

### TypeScript

| Snippet   | Purpose                                       |
|-----------|-----------------------------------------------|
| `bcmd`    | Create a standard Byron's command             |
| `bcmdp`   | Create a Byron's command with parent          |
| `bcmdi`   | Create a Byron's command with info            |
| `bcmdpi`  | Create a Byron's command with parent and info |
| `bhnd`    | Create a Byron's handler                      |

### YAML

| Snippet  | Purpose                                 |
|----------|-----------------------------------------|
| `bscnt`  | Declare a Byron content                 |
| `bsent`  | Declare a Byron entity                  |
| `bsipt`  | Declare a Byron input                   |
| `bscmd`  | Declare a Byron command                 |
| `bscmdp` | Declare a Byron command with parameters |
| `bshnd`  | Declare a Byron handler                 |
| `bsattr` | Declare a Byron atrribute               |
| `bspar`  | Declare a Byron parameter               |