# Change Log

All notable changes to the "byron-snippets" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [0.1.3]

- Add `async` to commands

## [0.1.2]

- Add new content snippets
- Fix spacing for yaml snippets

## [0.1.1]

- Lower necessary VSCode version

## [0.1.0]

- Initial beta release release, complait to Byron@0.3